# blast.R
#
# 2006-08-01
# Aron
#
# various functions related to BLAST
#

## read NCBI-BLAST results (assuming it was run with "-m 8" option)
read.blast <- function (file, nrows = -1, comment.char = '', fast = TRUE, ...) {
  if(fast && nrows == -1) {
    wc <- system(paste('wc', file), intern = TRUE)
    wc <- sub("^ +", "", wc)
    wc <- strsplit(wc, ' ')[[1]][1]
    nrows <- max(as.integer(wc), 1)
  } 
    read.delim(file, header = FALSE, comment.char = comment.char, 
        nrows = nrows, as.is = c(1, 2), col.names = c("queryID", 
            "subjectID", "percentId", "alignLength", "mismatches", 
            "gapOpenings", "qStart", "qEnd", "sStart", "sEnd", 
            "eVal", "bitScore"), 
        colClasses = c(rep('character', 2), rep('numeric', 10)), ...)
}


## reads WU-BLAST results (it must have been run with "-mformat 2" option)
read.wublast <- function (file, nrows = -1, comment.char = '', fast = TRUE, ...) {
  if(fast && nrows == -1) {
    wc <- system(paste('wc', file), intern = TRUE)
    wc <- sub("^ +", "", wc)
    wc <- strsplit(wc, ' ')[[1]][1]
    nrows <- max(as.integer(wc), 1)
  } 
    read.delim(file, header = FALSE, comment.char = comment.char, 
        nrows = nrows, as.is = c(1, 2), col.names = c("qid", 
            "sid", "E", "N", "Sprime", "S", "alignlen", "nident", 
            "npos", "nmism", "pcident", "pcpos", "qgaps", "qgaplen", 
            "sgaps", "sgaplen", "qframe", "qstart", "qend", "sframe", 
            "sstart", "send"), 
        colClasses = c(rep('character', 2), rep('numeric', 20)), ...) 
}


## read a FASTA formatted sequence file as a character vector.
read.fasta <- function (file, id.only = FALSE) 
{
    a <- readLines(file)
    if (length(a) > 0) {
        wh.hdrs <- grep(">", a)
        n <- length(wh.hdrs)
        headers <- sub(">", "", a[wh.hdrs])
        if(id.only) {
          headers <- sapply(strsplit(headers, " "), function(x) x[1])
        }
        seqStarts <- wh.hdrs + 1
        seqEnds <- c(wh.hdrs[-1] - 1, length(a))
        seq <- rep("", n)
        for (i in 1:n) {
            seq[i] <- paste(a[seqStarts[i]:seqEnds[i]], collapse = "")
        }
        names(seq) <- headers
        return(seq)
    }
    else {
        return(character(0))
    }
}


## write a FASTA formatted sequence file.
write.fasta <- function(x, file, headers = NULL, linelength = 50 ) {
  if(is.null(headers)) {
    headers <- as.character(seq_along(x))
  }
  stopifnot(length(x)==length(headers))
  stopifnot(all(!duplicated(headers)))
  pattern <- paste("(.{", linelength, "})(?!$)", sep="")
  joinedLines <- gsub(pattern, "\\1\n", x, perl=TRUE)
  fastaOut <- paste(">", headers, "\n", joinedLines, sep='') 
  write(fastaOut, file=file)
}


## run NCBI BLAST and collect results
blastall <- function(query, headers = NULL, args = "", run = TRUE, verbose = !run, ...) {
  if (length(query) == 1 && file.exists(query[1])) {
    queryFile <- query
  } else {
    queryFile <- tempfile()
    write.fasta(query, file = queryFile, headers = headers)
  }
  args <- paste(args, collapse = " ")
  m <- list(...)
  if(length(m) > 0) {
    otherArgs <- paste('-', names(m), ' ', m, sep='', collapse=' ')
  } else {
    otherArgs <- ''
  }
  outfile <- tempfile()
  myCall <- paste('blastall -m 8 -i', queryFile, args, otherArgs,
    '-o', outfile)
  if(verbose) 
    cat(myCall, "\n")
  if(run) {
     system(myCall)
     out <- read.blast(outfile)
     unlink(outfile)
     return(out)
   }
}


## run blastn (part of WU-BLAST) and collect results
blastn <- function (database, query, headers = NULL, args = "", mat = NULL, 
    run = TRUE, verbose = !run, ...) 
{
    if (length(query) == 1 && file.exists(query[1])) {
        queryFile <- query
    }
    else {
        queryFile <- tempfile()
        write.fasta(query, file = queryFile, headers = headers)
    }
    args <- paste(args, collapse = " ")
    m <- list(...)
    otherArgs <- paste(names(m), " ", m, sep = "", collapse = " ")
    if (!is.null(mat)) {
        stopifnot(length(mat) == 16)
        stopifnot(length(dimnames(mat)) == 2)
        stopifnot(rownames(mat) == colnames(mat))
        # hack to generate unique filenames under parallel R
        matfile <- paste(basename(tempfile()), Sys.getpid(), sep = '')
        matfile.2 <- paste(matfile, '.4.2', sep = '')
        matfile.4 <- paste(matfile, '.4.4', sep = '')
        write.table(mat, quote = FALSE, file = matfile.2)
        write.table(mat, quote = FALSE, file = matfile.4)
        on.exit(unlink(c(matfile.2, matfile.4)))
        matrixArgs <- paste('matrix=', matfile, sep = '')
    }
    else {
        matrixArgs <- ""
    }
    # hack to generate unique filenames under parallel R
    outfile <- paste(tempfile(),  Sys.getpid(), sep = '')
    myCall <- paste("blastn", database, queryFile, args, otherArgs, 
        matrixArgs, "-mformat 2", ">", outfile)
    if (verbose) 
        cat(myCall, "\n")
    if (run) {
      system(myCall)
      out <- read.wublast(outfile)
      unlink(outfile)
      return(out)
    }
}


xdformat <- function(x, headers = NULL, filename = tempfile()) {
  write.fasta(x, file = filename, headers = headers)
  system(paste('xdformat -n', filename))
  filename
}


formatdb <- function(x, headers = NULL, filename = tempfile()) {
  write.fasta(x, file = filename, headers = headers)
  system(paste('formatdb -p F -i', filename)) 
  filename
}


# get sequences from xd (wu-blast) database
xdget <- function(x, db) {
  cmd <- paste('xdget -n', db, paste(x, collapse = ' '))
  a <- system(cmd, intern = TRUE)
  if(length(a) > 0) {
    wh.hdrs <- grep(">", a)
    n <- length(wh.hdrs)
    stopifnot(n == length(x))
    header <- a[wh.hdrs]
    id <- sapply(header, function(y) sub('>', '', strsplit(y, ' ')[[1]][1]) )
    seqStarts <- wh.hdrs + 1
    seqEnds <- c(wh.hdrs[-1] - 1, length(a))
    seq <- rep('', n)
    for (i in 1:n) {
      seq[i] <- paste(a[seqStarts[i]:seqEnds[i]], collapse='')
    }
    names(seq) <- id
    return(seq)
  } else {
    return(character(0))
  }
}

