# blast #

## A simple R interface for NCBI-BLAST and WU-BLAST ##

This is an R package I wrote many years ago, but was never made public.

It has not been tested with any recent versions of BLAST, but it is possible that it might work.

This package contains functions to write FASTA files,
create BLAST databases, retrieve sequences from BLAST
databases, run BLAST searches, and import BLAST results into R.

This package is not available on CRAN.

## Installation ##

To install directly from Bitbucket, you can do this (in R):

	library(devtools)
	install_bitbucket("eklund/blast")